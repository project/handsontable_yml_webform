<?php

/**
 * @file
 * Contains \Drupal\theme_example\Element\MyElement.
 */

namespace Drupal\handsontable_yml_webform\Element;

use Drupal\Component\Utility\Html;
use Drupal\Core\Render\Element\FormElement;
use Drupal\handsontable_yml_webform\Form\LicenseTrait;

/**
 * Provides an example element.
 *
 * @RenderElement("handsontable")
 */
class Handsontable extends FormElement {

  use LicenseTrait;

  /**
   * @param string $folder where module is installed
   *
   * @return string if library is missing
   */
  public static function getLibInstructionsIfNeeded($folder) {
    if (\Drupal::hasService('library.libraries_directory_file_finder')) {
      /** @var \Drupal\Core\Asset\LibrariesDirectoryFileFinder $library_file_finder */
      $library_file_finder = \Drupal::service('library.libraries_directory_file_finder');
      if ($library_path = $library_file_finder->find('handsontable')) {
        $js = $library_path . '/handsontable/dist/handsontable.full.js';
        if (file_exists(\Drupal::root() . '/' . $js)) {

          return '';
        }
      }
    }
    if (!file_exists("$folder/js/handsontable.full.js") || !file_exists("$folder/css/handsontable.full.css")) {
      $msg = t('The Handsontable installation is incomplete, but no worries, you can fix this.') . ' ';
      $msg .= t('There are two ways to add the Handsontable source to Drupal: Composer and manual download. You can use the Composer way if you find the string "type:drupal-library" in your root composer.json file.') . ' ';
      $msg .= t('Add the package source to the "repositories" section of your composer.json - <a href="https://www.drupal.org/project/handsontable_yml_webform/issues/3278683#comment-14601528">look at the example by SocialNicheGuru</a>. Then run `composer require "handsontable/handsontable"`.');
      $msg .= '<br><br>';
      $msg .= t('Alternative:') . ' ';
      $msg .= t('Please download the Handsontable source zip from %s. Then place <ul><li>the file %s in the folder %s and <li>the file %s in the folder %s.</li></ul>');
      return '<b>' . t('Error') . '</b><br><br>' .
        sprintf($msg, "<a href='https://handsontable.com/download#tab-zip'>handsontable.com</a>", '<em>handsontable.full.js</em>', "<em>$folder/js</em>", '<em>handsontable.full.css</em>', "<em>$folder/css</em>") . '<br>' .
        t('Please note that the Handsontable source is <a href="https://www.drupal.org/node/422996">not allowed to be contained in the handsontable_yml_webform module</a>.');
    }
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#title' => '',
      '#pre_render' => [
        [$class, 'preRenderMyElement'],
      ],
    ];
  }

  /**
   * Prepare the render array for the template.
   *
   * @param array $element to be rendered (Form API)
   *
   * @return array
   */
  public static function preRenderMyElement($element) {
    static $iTable = 0;
    $result = [];
    if ($element['#title']) {
      $result['title'] = [
        '#type' => 'label',
        '#title' => $element['#title'],
        '#title_display' => '',
      ];
    }

    $aExtra = [];
    $aViewSettings = [];
    if (isset($element['#make_existing_data_read_only'])) {
      $aExtra['make_existing_data_read_only'] = TRUE;
    }
    if (isset($element['#background_colors'])) {
      $aExtra['background_colors'] = $element['#background_colors'];
    }
    if (isset($element['#initial_data'])) {
      $aExtra['initial_data'] = $element['#initial_data'];
    }
    $aExtra['licenseKey'] = $element['#license_key'] ?? self::getLicenseString();

    if (isset($element['#view_settings'])) {
      if (is_array($element['#view_settings'])) {
        // Form API
        $aViewSettings = $element['#view_settings'];
      }
      else {
        // Webform
        $aViewSettings = json_decode($element['#view_settings'], TRUE) ?: [];
      }
    }
    $settings = [
      'ids' => [$iTable => $element['#id']],
      'data' => [$iTable => !empty($element['#default_value']) ? $element['#default_value'] : NULL],
      'view_settings' => [$iTable => $aViewSettings + $aExtra],
    ];
    $iTable++;


    $result['#attached']['drupalSettings']['handsontable'] = $settings;

    $output = '';
    $output .= '<div class="handsontable-container">' . "\n";
    if (isset($aViewSettings['contextMenu']) && in_array('row_below', $aViewSettings['contextMenu'])) {
      $output .= '<a class="table-add-row" data-action="addRow" href="#addrow" alt="Add a row">Add row</a> ' . "\n";
    }
    if (isset($aViewSettings['contextMenu']) && in_array('col_right', $aViewSettings['contextMenu'])) {
      $output .= '<a class="table-add-col" data-action="addCol" href="#addcol" alt="Add a column">Add column</a> ' . "\n";
    }
    $output .= '<div id="' . $element['#id'] . '-table" class="handsontable"></div>' . "\n";
    $output .= '</div>' . "\n";

    $name = isset($element['#name']) ? $element['#name'] : $element['#id'];
    $value = isset($element['#default_value']) ? Html::escape($element['#default_value']) : '';
    $result['hidden'] = [
      '#type' => 'textfield',
      '#name' => $name,
      '#id' => $element['#id'],
      '#value' => $value,
      '#attributes' => array_merge($element['#attributes'], ['style' => 'display: none']),
    ];

    $folder = \Drupal::moduleHandler()->getModule('handsontable_yml_webform')->getPath();
    $output .= self::getLibInstructionsIfNeeded($folder);


    $result['handsontable'] = [
      '#type' => 'markup',
      '#markup' => $output,
      '#allowed_tags' => ['div', 'a', 'b', 'br', 'ul', 'li', 'em'],
      '#attached' => ['library' => ['handsontable_yml_webform/handsontable']],
    ];

    return $result;
  }

}
