<?php

namespace Drupal\handsontable_yml_webform\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class SettingsForm extends ConfigFormBase {

  use LicenseTrait;

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'handsontable_yml_webform.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['help']['#markup'] = $this->t('On this page you can set the default settings for new webform elements.');

    $form['license_key'] = $this->getLicenseKeyTextField();
    $form['license_key']['#default_value'] = self::getLicenseString();

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('handsontable_yml_webform.settings')
      ->set('license_key', $form_state->getValue('license_key'))
      ->save();
  }

}
