<?php

namespace Drupal\handsontable_yml_webform\Form;

use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\TranslatableMarkup;

trait LicenseTrait {

  private function getTextFromUrl(string $url): TranslatableMarkup {
    return t('See @url for details.', ['@url' => Markup::create(sprintf('<a target="_blank" href="%1$s">%1$s</a>', $url))]);
  }

  private function getLicenseKeyTextField(): array {
    return [
      '#type' => 'textfield',
      '#title' => $this->t('License key'),
      '#description' => t('Sets the <code>"licenseKey"</code> value for Handsontable.') . ' ' . $this->getTextFromUrl('https://handsontable.com/docs/license-key'),
    ];
  }

  public static function getLicenseString(): string {
    return \Drupal::config('handsontable_yml_webform.settings')->get('license_key') ?: 'non-commercial-and-evaluation';
  }

}
