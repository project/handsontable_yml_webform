<?php

use Drupal\Core\Render\Markup;
use Drupal\handsontable_yml_webform\Element\Handsontable;

/**
 * Implements hook_requirements().
 */
function handsontable_yml_webform_requirements() {
  $sError = Handsontable::getLibInstructionsIfNeeded(__DIR__);
  if (!$sError) {
    return [];
  }

  return [
    'handsontable_yml_webform' => [
      'title' => t('Handsontable For YML Webform'),
      'value' => t('There are files missing.'),
      'severity' => REQUIREMENT_ERROR,
      'description' => Markup::create($sError),
    ],
  ];
}

/**
 * Implements hook_library_info_alter().
 *
 * Allows adding Handsontable as a package to the "repositories"
 * section of the root composer.json.
 *
 * @see
 * https://www.drupal.org/project/handsontable_yml_webform/issues/3278683#comment-14601528
 */
function handsontable_yml_webform_library_info_alter(&$libraries, $extension) {
  if ($extension == 'handsontable_yml_webform' && isset($libraries['handsontable'])) {

    if (Drupal::hasService('library.libraries_directory_file_finder')) {
      /** @var \Drupal\Core\Asset\LibrariesDirectoryFileFinder $library_file_finder */
      $library_file_finder = Drupal::service('library.libraries_directory_file_finder');
      $library_path = $library_file_finder->find('handsontable');
      if (!$library_path) {
        return;
      }
      $js = '/' . $library_path . '/handsontable/dist/handsontable.full.js';
      $css = '/' . $library_path . '/handsontable/dist/handsontable.full.css';
      if (!file_exists(Drupal::root() . $js) || !file_exists(Drupal::root() . $css)) {
        return;
      }
      $libraries['handsontable']['js'] = [
        $js => [],
        'js/handsontable.config.js' => [],
      ];
      $libraries['handsontable']['css']['component'] = [
        $css => [],
        'css/handsontable.custom.css' => [],
      ];
    }
  }
}
